import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function Square(props) {
    return (
      <button 
          className="square"
          onClick={props.onClick}>
          {props.value}
      </button>
    );
}

function Board(props){

  function renderSquare(i) {
    return <Square onClick={() => props.onClick(i)} value={props.values[i]}/>;
  }  
  return (
    <div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
}

class Game extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        history : [Array(9).fill("")],
        xIsNext : true,
        stepNumber : 0,
    }
  }
  handleClick = (i) => {
    const history = this.state.history.slice(0, this.state.stepNumber+1);
    const values = history[this.state.stepNumber].slice();
    if(calculateWinner(values) || values[i]){
      return null;
    }
    this.state.xIsNext ? values[i] = "X" : values[i] = "O";
    history.push(values);
    this.setState({
        history,
        xIsNext : !this.state.xIsNext,
        stepNumber : this.state.stepNumber +1,
      })
  }
  
  goToMove(move) {
    this.setState({
      stepNumber : move,
      xIsNext : (move % 2) === 0,
    });
  }

  render() {
    let status;
    const winner = calculateWinner(this.state.history[this.state.stepNumber]);
    if(winner){
      status = "The grand winner IIIIIS : " + (winner === "X" ? "Badr (¬‿¬) :o" : "Julie ( ͡° ͜ʖ ͡°)" );
    }
    else{
      status = 'Next player : ' + (this.state.xIsNext ? 'Badr (X)' : 'Julie (O)');
    }
    let moves;
    moves = this.state.history.map((step, move) => {
      const desc = move ? "Go to move #"+move : "Go to game start";
      return (
        <li key={move}>
          <button onClick={() => this.goToMove(move)}>{desc}</button>
        </li>
      );
    })

    return (
      <div className="game">
        <div className="game-board">
          <Board values = {this.state.history[this.state.stepNumber]} onClick={this.handleClick}/>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}


// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
